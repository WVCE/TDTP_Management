<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Groupes.Groupe');
});

Route::get('/Groupes', function () {
    return view('Groupes.Groupe');
});

Route::get('/TDTP', function () {
    return view('TDTP.TdTp');
});

Route::get('/Etudiants', function () {
    return view('Etudiant.Etudiant');
});

Route::get('/Niveaux', function () {
    return view('Niveaux.Niveau');
});

Route::get('/Charges', function () {
    return view('Charger.Charge');
});

Route::get('/UVs', function () {
    return view('UV.uv');
});

Route::get('/Salles', function () {
    return view('Salles.Salle');
});

