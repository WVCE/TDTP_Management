@extends('Apps.App')

@section('menu');
	<div class="alert alert-success" role="alert">
		<span>
			<strong>Alert</strong> text.
		</span>
        <button class="close"><span aria-hidden="true">×</span></button>
    </div>
	<div class="container-fluid">
		 <div>
	            <ul class="nav nav-tabs nav-justified pane">
	                <li><a class="text-primary" href="{{url('Salles')}}" role="tab">Salles (0)</a></li>
	                <li><a class="text-primary" href="{{url('UVs')}}" role="tab" >UV (0)</a></li>
	                <li><a class="text-primary" href="{{url('Charges')}}" role="tab" >Chargés (0)</a></li>
	                <li><a class="text-primary" href="{{url('Etudiants')}}" role="tab" >Etudiants (0)</a></li>
	                <li><a class="text-primary" href="{{url('Niveaux')}}" role="tab" >Niveaux (0)</a></li>
	                <li><a class="text-primary" href="{{url('TDTP')}}" role="tab" >TD/TP (0)</a></li>
	                <li><a class="text-primary" href="{{url('Groupes')}}" role="tab" >Groupes (0)</a></li>
	            </ul>
	            <div class="tab-content"></div>
	     </div>
	</div>
@endsection

@section('premier contenu');


      	@yield('contenu')


@endsection