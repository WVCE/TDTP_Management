@extends('Apps.Home')

@section('contenu')

	<div>
        <div class="container-fluid pane">
            <div class="col-md-6">
            	<h3> TD </h3>
                <div class="panel-group" role="tablist" aria-multiselectable="true" id="accordion-1">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" aria-expanded="true" href="#accordion-1 .item-1">Groupes 3</a></h4></div>
                        <div class="panel-collapse collapse in item-1" role="tabpanel">
                            <div class="panel-body"><span>Groupes 2.</span></div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" aria-expanded="false" href="#accordion-1 .item-2">Accordion Item</a></h4></div>
                        <div class="panel-collapse collapse item-2" role="tabpanel">
                            <div class="panel-body"><span>Item body.</span></div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-1" aria-expanded="false" href="#accordion-1 .item-3">Accordion Item</a></h4></div>
                        <div class="panel-collapse collapse item-3" role="tabpanel">
                            <div class="panel-body"><span>Item body.</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            	<h3> TP </h3>
                <div class="panel-group" role="tablist" aria-multiselectable="true" id="accordion-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-2" aria-expanded="true" href="#accordion-2 .item-1">Groupes 3</a></h4></div>
                        <div class="panel-collapse collapse in item-1" role="tabpanel">
                            <div class="panel-body"><span>Groupes 2.</span></div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-2" aria-expanded="false" href="#accordion-2 .item-2">Accordion Item</a></h4></div>
                        <div class="panel-collapse collapse item-2" role="tabpanel">
                            <div class="panel-body"><span>Item body.</span></div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab">
                            <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion-2" aria-expanded="false" href="#accordion-2 .item-3">Accordion Item</a></h4></div>
                        <div class="panel-collapse collapse item-3" role="tabpanel">
                            <div class="panel-body"><span>Item body.</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection