
$(document).ready(function() {
	
	$('.close').click(function(){
		$(this).parent().hide('slow');
	});

	$('.btn-xs').click(function(){
		var id = $(this).attr('id');
		addItem(id);
	});

	$('li').click(function(){
		$(document).find('li').removeAttr('class');
		$(this).attr('class','active');
	});	
});